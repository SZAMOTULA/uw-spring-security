package edu.wisc.uwss.web;

import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.UWUserDetailsImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import javax.servlet.ServletException;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Tests for {@link PviAttributeBindingFilter}
 *
 * @author apsummers
 */
public class PviAttributeBindingFilterTest {

    private PviAttributeBindingFilter filter;
    private MockHttpServletRequest req;
    private MockHttpServletResponse res;
    private MockFilterChain filterChain;

    @Before
    public void setUp() {
        filter = new PviAttributeBindingFilter();
        req = new MockHttpServletRequest();
        res = new MockHttpServletResponse();
        filterChain = new MockFilterChain();
    }

    @Test
    public void testDoFilter() throws ServletException, IOException {
        // Create a UWUserDetails to represent the requesting user
        UWUserDetails principal = new UWUserDetailsImpl("UW000A000", "admin", "password", "Amy Admin", "amy.admin@wisc.edu");
        PreAuthenticatedAuthenticationToken preauthToken = new PreAuthenticatedAuthenticationToken(principal, null);

        // Setup security context
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(preauthToken);
        SecurityContextHolder.setContext(securityContext);
        
        filter.doFilter(req, res, filterChain);
        assertEquals("UW000A000", req.getAttribute("wiscedupvi"));
    }

}
