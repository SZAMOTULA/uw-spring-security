/**
 * 
 */
package edu.wisc.uwss.impersonation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Tests for {@link SwitchUserOnHeaderFilter}.
 * 
 * @author Nicholas Blair
 */
public class SwitchUserOnHeaderFilterTest {

  private SwitchUserOnHeaderFilter filter = new SwitchUserOnHeaderFilter();
  @Before
  public void clearAuthentication() {
    // guarantee we are not authenticated by clearing the context
    SecurityContextHolder.clearContext();
  }
  /**
   * Confirm stable behavior for {@link SwitchUserOnHeaderFilter#requiresSwitchUser(javax.servlet.http.HttpServletRequest)}
   * when no {@link Authentication} present.
   * 
   * @throws IOException
   * @throws ServletException
   */
  @Test
  public void requiresSwitchUser_failure_unauthenticated() throws IOException, ServletException {
    MockHttpServletRequest req = new MockHttpServletRequest();
    
    assertFalse(filter.requiresSwitchUser(req));
  }
  /**
   * Confirm behavior for {@link SwitchUserOnHeaderFilter#requiresSwitchUser(javax.servlet.http.HttpServletRequest)}
   * when {@link Authentication} present but missing required {@link GrantedAuthority}.
   * @throws IOException
   * @throws ServletException
   */
  @Test
  public void requiresSwitchUser_failure_headerPresent_but_missing_grantedAuthority() throws IOException, ServletException {
    Authentication authentication = new UsernamePasswordAuthenticationToken("foo", "bar", Arrays.asList(new SimpleGrantedAuthority[] { }));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    MockHttpServletRequest req = new MockHttpServletRequest();
    req.addHeader(filter.getSwitchUserHeaderName(), "someone");
    assertFalse(filter.requiresSwitchUser(req));
  }
  /**
   * Confirm success for {@link SwitchUserOnHeaderFilter#requiresSwitchUser(javax.servlet.http.HttpServletRequest)}
   * when {@link Authentication} present and has required {@link GrantedAuthority}.
   * @throws IOException
   * @throws ServletException
   */
  @Test
  public void requiresSwitchUser_success_authorized() throws IOException, ServletException {
    Authentication authentication = new UsernamePasswordAuthenticationToken("foo", "bar", 
        Arrays.asList(new SimpleGrantedAuthority[] { new SimpleGrantedAuthority(filter.getRequiredGrantedAuthority()) }));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    MockHttpServletRequest req = new MockHttpServletRequest();
    req.addHeader(filter.getSwitchUserHeaderName(), "someone");
    assertTrue(filter.requiresSwitchUser(req));
  }
}
