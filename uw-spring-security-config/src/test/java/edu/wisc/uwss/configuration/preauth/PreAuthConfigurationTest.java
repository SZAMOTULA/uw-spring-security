package edu.wisc.uwss.configuration.preauth;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.Configuration;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import edu.wisc.uwss.preauth.UWUserDetailsAuthenticationFilter;
import edu.wisc.uwss.preauth.UWUserDetailsAuthenticationUserDetailsService;

/**
 * A unit test that activates the {@link Configuration} classes in the "edu.wisc.uwss.configuration.preauth" package.
 * 
 * @author Nicholas Blair
 */
public class PreAuthConfigurationTest {

  private AnnotationConfigWebApplicationContext rootContext;
  
  /**
   * Construct a fresh context, scan for components in the package, and set the necessary active profiles.
   * Done in {@link Before} in case we want to create additional configurations/combinations in separate tests later.
   */
  @Before
  public void setup() {
    rootContext = new AnnotationConfigWebApplicationContext();
    rootContext.setServletContext(new MockServletContext());
    rootContext.scan("edu.wisc.uwss.configuration.preauth");
    rootContext.getEnvironment().setActiveProfiles("preauth");
  }
  
  /**
   * Initialize the application context with this package's {@link Configuration} classes.
   */
  @Test
  public void initContext() {
    rootContext.refresh();
    assertTrue(rootContext.isActive());
    assertTrue(rootContext.isRunning());
    assertNotNull(rootContext.getBean(UWUserDetailsAuthenticationUserDetailsService.class));
    assertNotNull(rootContext.getBean(UWUserDetailsAuthenticationFilter.class));
  }
}
