/**
 * 
 */
package edu.wisc.uwss.configuration.preauth;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;

import com.github.nblair.web.PreAuthenticationSimulationServletFilter;
import com.github.nblair.web.ProfileConditionalDelegatingFilterProxy;

import edu.wisc.uwss.configuration.HttpSecurityAmender;
import edu.wisc.uwss.configuration.development.SimulatePreAuthenticationHttpSecurityAmender;

/**
 * Isolate PreAuthenticationServletFilter configuration.
 * 
 * Activated with the "preauth-dev" profile.
 * 
 * @author Collin Cudd
 */
@Configuration
@Profile({ "preauth-dev", "edu.wisc.uwss.preauth-dev" })
public class PreAuthenticationDevelopmentConfiguration {

  private static final Logger logger = LoggerFactory.getLogger(PreAuthenticationDevelopmentConfiguration.class);
  
  @Inject
  Environment environment;
  /**
   * @throws Exception
   */
  @Bean
  public HttpSecurityAmender httpSecurityAmender() throws Exception {
    logger.warn("'preauth-dev' profile activated, setting up PreAuthenticationSimulationServletFilter. Confirm this not a production environment!");
    return new SimulatePreAuthenticationHttpSecurityAmender(preAuthenticationSimulationFilter());
  }
  
  /**
   * @return
   * @throws ServletException
   */
  @Bean
  public Filter preAuthenticationSimulationFilter() throws ServletException {
      PreAuthenticationSimulationServletFilter filter = new PreAuthenticationSimulationServletFilter();
      filter.setRemoteUser(environment.getProperty("preauth.remoteUser"));
      filter.setAdditionalHeaders(environment.getProperty("preauth.headerNames"), environment.getProperty("preauth.headerValues"));
      return new ProfileConditionalDelegatingFilterProxy(filter).setRequiredProfile("preauth-dev");
  }
}
