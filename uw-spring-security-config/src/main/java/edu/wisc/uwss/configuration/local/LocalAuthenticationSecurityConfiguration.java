/**
 * 
 */
package edu.wisc.uwss.configuration.local;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.provisioning.UserDetailsManager;

import edu.wisc.uwss.local.LocalUserDetailsManagerImpl;

/**
 * @author Nicholas Blair
 */
@Configuration
@Profile({"local-users", "edu.wisc.uwss.local-users"})
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class LocalAuthenticationSecurityConfiguration extends GlobalMethodSecurityConfiguration implements ResourceLoaderAware {

  private ResourceLoader resourceLoader;
  
  @Override
  public void setResourceLoader(ResourceLoader resourceLoader) {
    this.resourceLoader = resourceLoader;
  }
  
  private Logger logger = LoggerFactory.getLogger(getClass());
  /* (non-Javadoc)
   * @see org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration#configure(org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder)
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(localUserDetailsManager());
    logger.debug("injected localUserDetailsService");
  }
  /**
   * @return {@link UserDetailsManager}.
   */
  @Bean
  public UserDetailsManager localUserDetailsManager() {
    return new LocalUserDetailsManagerImpl();
  }



}
