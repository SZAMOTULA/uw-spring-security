package edu.wisc.uwss.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * There can be only one {@link WebSecurityConfigurerAdapter}.
 *
 * @author Nicholas Blair
 */
@Configuration
@EnableWebSecurity
public class UWSpringSecurityWebConfigurerAdapter extends WebSecurityConfigurerAdapter {

  protected final Logger logger = LoggerFactory.getLogger(this.getClass());
  @Autowired(required=false)
  List<HttpSecurityAmender> amenders = new ArrayList<>();

  /**
   * {@inheritDoc}
   *
   * Fires {@link HttpSecurityAmender#amend(HttpSecurity)} on all instances registered in the
   * ApplicationContext.
   */
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    logger.debug("configuring {} HttpSecurityAmenders", amenders.size());
    for(HttpSecurityAmender amender: amenders) {
      amender.amend(http);
      logger.debug("configured HttpSecurityAmender {}", amender.toString());
    }
  }
}
