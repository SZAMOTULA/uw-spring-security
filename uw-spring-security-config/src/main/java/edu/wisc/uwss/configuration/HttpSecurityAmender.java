/**
 * 
 */
package edu.wisc.uwss.configuration;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Callback interface provided to allow downstream projects to inject changes to the {@link HttpSecurity}
 * provided by {@link WebSecurityConfigurerAdapter} AT THE RIGHT TIME, before it is closed off to change.
 * 
 * {@link UWSpringSecurityWebConfigurerAdapter} depends on this interface and
 * will invoke all implementing instances it at the end of {@link WebSecurityConfigurerAdapter#configure(HttpSecurity)}.
 * 
 * @author Nicholas Blair
 */
public interface HttpSecurityAmender {

  /**
   * Amend the {@link HttpSecurity} how you see fit. Typically used when NOT using {@link EverythingRequiresAuthenticationConfiguration}.
   *
   * Some use cases:
   * <ul>
   *   <li>Specifying authentication or authorization rules for specific URLs within your application.</li>
   *   <li>Internally by local-users and preauth to inject the appropriate spring security services.</li>
   * </ul>
   * 
   * @param httpSecurity
   * @throws Exception
   */
  void amend(HttpSecurity httpSecurity) throws Exception;
  
  /**
   * Default implementation makes no changes.
   * 
   * @author Nicholas Blair
   */
  public static class Default implements HttpSecurityAmender {

    /**
     * {@inheritDoc}
     * 
     * No changes made.
     */
    public void amend(HttpSecurity httpSecurity) {
    }
    
  }
}
