package edu.wisc.uwss.local;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import edu.wisc.uwss.UWUserDetails;

/**
 * Special {@link UWUserDetails} type used by tests in sibling classes.
 * 
 * Intentionally does not implement HasModifiableSource.
 * 
 * @author Nicholas Blair
 */
final class NotHasModifiableSourceUWUserDetails implements UWUserDetails {
  /**
   * 
   */
  private static final long serialVersionUID = 9164913061422326802L;

  private final String username;
  
  NotHasModifiableSourceUWUserDetails(String username) {
    this.username = username;
  }
  @Override
  public boolean isEnabled() { return false; }
  @Override
  public boolean isCredentialsNonExpired() { return false; }
  @Override
  public boolean isAccountNonLocked() { return false; }
  @Override
  public boolean isAccountNonExpired() { return false; }
  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public String getPassword() { 
    return ""; 
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() { return null; }

  @Override
  public Collection<String> getUddsMembership() { return null; }

  @Override
  public String getSource() { return null; }

  @Override
  public String getPvi() { return null; }

  @Override
  public String getLastName() { return null; }

  @Override
  public String getIsisEmplid() { return null; }

  @Override
  public String getFullName() { return null; }

  @Override
  public String getDisplayName() { return null; }

  @Override
  public String getFirstName() { return null; }

  @Override
  public String getEppn() { return null; }

  @Override
  public String getEmailAddress() { return null; }

  @Override
  public String getCustomLogoutUrl() { return null; }
}