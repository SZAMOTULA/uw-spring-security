/**
 * 
 */
package edu.wisc.uwss.local;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import edu.wisc.uwss.HasModifiableSource;
import edu.wisc.uwss.UWUserDetails;
import edu.wisc.uwss.UWUserDetailsImpl;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * Tests for {@link LocalUserDetailsManagerImpl}.
 * @author Collin Cudd
 */
public class LocalUserDetailsManagerImplTest {

  /**
   * Verify {@link LocalUserDetailsManagerImpl#loadUserByUsername(String)} throws
   * {@link UsernameNotFoundException} for an unknown user.
   */
  @Test(expected=UsernameNotFoundException.class)
  public void loadUserByUsername_notfound() {
    LocalUserDetailsManagerImpl service = new LocalUserDetailsManagerImpl();
      service.loadUserByUsername("notthere");
  }
  
  /**
   * We need to make sure we don't return the only instance of the 
   * {@link UWUserDetails}, as Spring Security will mutate it.
   */
  @Test
  public void loadUserByUsername_not_same_instance() {
      LocalUserDetailsManagerImpl service = new LocalUserDetailsManagerImpl();
      UWUserDetails originalDemoUser = new UWUserDetailsImpl("UW000A000", "foo", "bar", "Foo Bar", "foo@foo.wisc.edu");
      service.addDemoUser(originalDemoUser);
      
      UserDetails retrieved = service.loadUserByUsername("foo");
      // .equals() should be true (usernames are the same)
      assertEquals(originalDemoUser, retrieved);
      // == MUST NOT be true (need to be 2 different instances
      assertFalse(retrieved == originalDemoUser);
      // verify repeated invocations are also new instances
      assertFalse(retrieved == service.loadUserByUsername("foo"));
  }
  /**
   * Control experiment for {@link LocalUserDetailsManagerImpl}.
   * Expects the "admin" user with "Amy Administrator" fullName.
   * 
   * @throws IOException 
   */
  @Test
  public void init_demoUsers_control() throws IOException {
      LocalUserDetailsManagerImpl service = new LocalUserDetailsManagerImpl();
      service.setLocalUserResource(new ClassPathResource("test-users.yaml"));
      service.init();
      
      UWUserDetails userDetails = service.loadUserByUsername("admin");
      assertNotNull(userDetails);
      assertEquals("UW000A000", userDetails.getPvi());
      assertEquals("Amy Administrator", userDetails.getFullName());
      
      UWUserDetails jane = service.loadUserByUsername("jane");
      assertEquals("UW000A001", jane.getPvi());
      assertEquals("Jane Doe", jane.getFullName());
      assertEquals("Loretta Doe", jane.getDisplayName());

      UWUserDetails john = service.loadUserByUsername("john");
      assertEquals("UW000A004", john.getPvi());
      assertEquals("John Doe", john.getFullName());
      assertEquals("Jack Doe", john.getDisplayName());

      assertTrue(john.getUddsMembership().contains("A535005"));
  }

  /**
   * Set up a properties instance including a user with a single UDDS.
   * Verify that value properly set in {@link UWUserDetails#getUddsMembership()}.
   */
  @Test
  public void init_demoUsers_with_no_udds_and_no_control() {
      LocalUserDetailsManagerImpl service = new LocalUserDetailsManagerImpl();
      service.setLocalUserResource(new ClassPathResource("test-users.yaml"));
      service.init();
      
      UWUserDetails userDetails = service.loadUserByUsername("testnoudds");
      assertNotNull(userDetails);
      assertEquals("UW000A003", userDetails.getPvi());
      assertEquals("Nothing", userDetails.getFullName());
      assertTrue(userDetails.getUddsMembership().isEmpty());
  }
  /**
   * Set up a properties instance including a user with a single UDDS.
   * Verify that value properly set in {@link UWUserDetails#getUddsMembership()}.
   */
  @Test
  public void init_demoUsers_with_single_udds() {
      LocalUserDetailsManagerImpl service = new LocalUserDetailsManagerImpl();
      service.setLocalUserResource(new ClassPathResource("test-users.yaml"));
      service.init();
      
      UWUserDetails userDetails = service.loadUserByUsername("testsingleudds");
      assertNotNull(userDetails);
      assertEquals("UW000A002", userDetails.getPvi());
      assertEquals("Single UDDS", userDetails.getFullName());
      assertTrue(userDetails.getUddsMembership().contains("A535005"));
  }
  /**
   * Set up a properties instance including a user with multiple UDDS values.
   * Verify that values properly set in {@link UWUserDetails#getUddsMembership()}.
   */
  @Test
  public void setDemoUsers_with_multiple_udds() {
      LocalUserDetailsManagerImpl service = new LocalUserDetailsManagerImpl();
      service.setLocalUserResource(new ClassPathResource("test-users.yaml"));
      service.init();
      
      UWUserDetails userDetails = service.loadUserByUsername("testmultiudds");
      assertNotNull(userDetails);
      assertEquals("UW000B003", userDetails.getPvi());
      assertEquals("Multiple UDDS", userDetails.getFullName());
      assertTrue(userDetails.getUddsMembership().contains("A535005"));
      assertTrue(userDetails.getUddsMembership().contains("A535900"));
      assertTrue(userDetails.getUddsMembership().contains("A061236"));
  }
  
  /**
   * Control test for {@link LocalUserDetailsManagerImpl#createUser(UserDetails)}.
   */
  @Test
  public void createUser_control() {
    LocalUserDetailsManagerImpl manager = new LocalUserDetailsManagerImpl();
    String username = "username";
    assertFalse(manager.userExists(username));
    UserDetails user = new UWUserDetailsImpl("pvi", username, "password", "fullName", "emailAddress");
    manager.createUser(user);
    assertTrue(manager.userExists(username));
    assertEquals("edu.wisc.uwss.local-users", manager.loadUserByUsername("username").getSource());
  }
  
  /**
   * Test for {@link LocalUserDetailsManagerImpl#createUser(UserDetails)} when the
   * {@link UWUserDetails} instance does not implement {@link HasModifiableSource}.
   * 
   * Manager should persist the instance, but no dispatch to setSource occurs, so the field is null.
   */
  @Test
  public void createUser_userDoesNotImplementHasModifableSource() {
    LocalUserDetailsManagerImpl manager = new LocalUserDetailsManagerImpl();
    String username = "notHasModifiableSource";
    assertFalse(manager.userExists(username));
    
    UWUserDetails user = new NotHasModifiableSourceUWUserDetails(username);

    manager.createUser(user);
    assertTrue(manager.userExists(username));
    assertNull(manager.loadUserByUsername(username).getSource());
  }
  /**
   * Verify an {@link IllegalArgumentException} is thrown when
   * {@link LocalUserDetailsManagerImpl#createUser(UserDetails)} called with a {@link UserDetails} that
   * has an empty username field.
   */
  @Test(expected=IllegalArgumentException.class)
  public void createUser_noUsername() {
    LocalUserDetailsManagerImpl manager = new LocalUserDetailsManagerImpl();
    UserDetails user = new UWUserDetailsImpl("pvi", "", "password", "fullName", "emailAddress");
    manager.createUser(user);
  }
  
  @Test(expected=IllegalArgumentException.class)
  public void createUser_not_a_UWUserDetails() {
    LocalUserDetailsManagerImpl manager = new LocalUserDetailsManagerImpl();
    UserDetails user = new UserDetails() {
      /**
       * 
       */
      private static final long serialVersionUID = 852103757007714784L;

      @Override
      public boolean isEnabled() {
        return false;
      }
      
      @Override
      public boolean isCredentialsNonExpired() {
        return false;
      }
      
      @Override
      public boolean isAccountNonLocked() {
        return false;
      }
      
      @Override
      public boolean isAccountNonExpired() {
        return false;
      }
      
      @Override
      public String getUsername() {
        return "user";
      }
      
      @Override
      public String getPassword() {
        return null;
      }
      
      @Override
      public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
      }
    };
    manager.createUser(user);
  }
  /**
   * Control test for {@link LocalUserDetailsManagerImpl#updateUser(UserDetails)}
   */
  @Test
  public void updateUser_control() {
    LocalUserDetailsManagerImpl manager = new LocalUserDetailsManagerImpl();
    String username = "username";
    assertFalse(manager.userExists(username));
    String fullName = "fullName";
    UserDetails user = new UWUserDetailsImpl("pvi", username, "password", fullName, "emailAddress");
    manager.createUser(user);
    assertTrue(manager.userExists(username));
    UWUserDetails createdUser = manager.loadUserByUsername("username");
    assertNotNull(createdUser);
    assertEquals(username, user.getUsername());
    assertEquals(fullName, createdUser.getFullName());
    UserDetails user2 = new UWUserDetailsImpl("pvi", username, "password", fullName+"2", "emailAddress");
    manager.updateUser(user2);
    UWUserDetails updatedUser = manager.loadUserByUsername("username");
    assertEquals(user2, updatedUser);
    assertEquals(fullName+"2", updatedUser.getFullName());
  }
  
  /**
   * Control test for {@link LocalUserDetailsManagerImpl#deleteUser(String)}
   */
  @Test
  public void deleteUser_control() {
    LocalUserDetailsManagerImpl manager = new LocalUserDetailsManagerImpl();
    String username = "username";
    assertFalse(manager.userExists(username));
    UserDetails user = new UWUserDetailsImpl("pvi", username, "password", "fullName", "emailAddress");
    manager.createUser(user);
    assertTrue(manager.userExists(username));
    
    manager.deleteUser(username);
    assertFalse(manager.userExists(username));
  }

  /**
   * Test confirming {@link LocalUsersAuthenticationAttemptCallback} is fired appropriately
   * during an authentication attempt ({@link LocalUserDetailsManagerImpl#loadUserByUsername(String)}).
   */
  @Test
  public void loadUserByUsername_with_callback() {
    LocalUserDetailsManagerImpl manager = new LocalUserDetailsManagerImpl();
    // setup a simple callback to mutate the userDetails
    LocalUsersAuthenticationAttemptCallback callback = new LocalUsersAuthenticationAttemptCallback<UWUserDetailsImpl>() {
      @Override
      public void success(UWUserDetailsImpl userDetails) {
        userDetails.setFirstName("something custom");
      }
    };
    manager.setCallbacks(Arrays.asList(callback));

    // no first name set on original user
    UWUserDetails user = new UWUserDetailsImpl("UW000A000", "foo", "bar", "Foo Bar", "foo@foo.wisc.edu");
    // first name starts null
    assertNull(user.getFirstName());
    manager.addDemoUser(user);

    UWUserDetails result = manager.loadUserByUsername("foo");
    // observe firstname modified by the callback
    assertEquals("something custom", result.getFirstName());
  }

  @Test
  public void unsupportedFormatWithUDDS() {
    LocalUserDetailsLoader attributesMapper = new LocalUserDetailsLoader.Default();
    List<UWUserDetails> users = attributesMapper.loadUsers(new ClassPathResource("test-users.json"));

    //demo STAR user with UDDS:
    String username = "aalpaca";

    UWUserDetails uwUserDetails = users.get(0);
    assertEquals(username, uwUserDetails.getUsername());
    assertEquals("aalpaca", uwUserDetails.getPassword());
    assertEquals("UW123D455", uwUserDetails.getPvi());
    assertEquals("Amy Alpaca", uwUserDetails.getFullName());
    assertEquals("amy.alpaca@demo.wisc.edu", uwUserDetails.getEmailAddress());
    assertTrue(uwUserDetails.getUddsMembership().contains("A064079"));

  }

  @Test
  public void unsupportedFormatWithoutUDDS() {
    LocalUserDetailsLoader attributesMapper = new LocalUserDetailsLoader.Default();
    List<UWUserDetails> users = attributesMapper.loadUsers(new ClassPathResource("test-users.json"));
    //demo STAR user without UDDS:
    String username = "jim";

    UWUserDetails uwUserDetails = users.get(1);
    assertEquals(username, uwUserDetails.getUsername());
    assertEquals("jim", uwUserDetails.getPassword());
    assertEquals("UW000A003", uwUserDetails.getPvi());
    assertEquals("Jim Doe", uwUserDetails.getFullName());
    assertEquals("jim.doe@demo.wisc.edu", uwUserDetails.getEmailAddress());
    assertTrue(uwUserDetails.getUddsMembership().isEmpty());

  }
}
  