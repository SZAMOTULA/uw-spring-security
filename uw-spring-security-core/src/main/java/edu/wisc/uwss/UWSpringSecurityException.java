package edu.wisc.uwss;

/**
 * Super type for all {@link RuntimeException}s thrown by UW-Spring-Security.
 *
 * @author Nicholas Blair
 */
public class UWSpringSecurityException extends RuntimeException {
  public UWSpringSecurityException() {
  }

  public UWSpringSecurityException(String message) {
    super(message);
  }

  public UWSpringSecurityException(String message, Throwable cause) {
    super(message, cause);
  }

  public UWSpringSecurityException(Throwable cause) {
    super(cause);
  }
}
