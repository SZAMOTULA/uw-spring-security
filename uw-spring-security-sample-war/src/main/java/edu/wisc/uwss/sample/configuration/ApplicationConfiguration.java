/**
 * 
 */
package edu.wisc.uwss.sample.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

/**
 * Root {@link Configuration} for this sample project.
 * 
 * Uses {@link PropertySource} to load the sample preauthentication
 * properties into the {@link Environment}.
 * 
 * @author Nicholas Blair
 */
@Configuration
@PropertySources({
        @PropertySource("classpath:edu/wisc/uwss/configuration/uwss-SAMPLE.properties"),
        @PropertySource("classpath:edu/wisc/uwss/configuration/sample-war.properties")
})
public class ApplicationConfiguration {

  /**
   * This bean is required for {@link Value} annotations to be able to accept placeholders.
   * 
   * @return a {@link PropertySourcesPlaceholderConfigurer}
   */
  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
      return new PropertySourcesPlaceholderConfigurer();
  }
}
