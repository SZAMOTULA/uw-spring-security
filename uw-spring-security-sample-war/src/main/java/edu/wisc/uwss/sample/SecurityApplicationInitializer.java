/**
 * 
 */
package edu.wisc.uwss.sample;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Spring Security application initializer. This class seems weird, but it's definitely necessary.
 * See <a href="http://docs.spring.io/spring-security/site/docs/3.2.4.RELEASE/reference/htmlsingle/#abstractsecuritywebapplicationinitializer-with-spring-mvc">http://docs.spring.io/spring-security/site/docs/3.2.4.RELEASE/reference/htmlsingle/#abstractsecuritywebapplicationinitializer-with-spring-mvc</a>.
 * 
 * @author Nicholas Blair
 */
public class SecurityApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
